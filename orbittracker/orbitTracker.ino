#include <AccelStepper.h>
#include "MPU9250.h"
#include "EEPROM.h"

#define sign(x) ((x) < 0 ? -1 : ((x) > 0 ? 1 : 0))

/* Position */
struct Coord
{
	float Lat;
	float Lng;
};

// Upper arm (pitch) goes from 0 (pointing down) to 180 (pointing up) to 360 (pointing down again)
// Lower arm (yaw) goes from 0 (pointing towards wires) to 180 (pointing away from wires) to 360 (pointing towards wires again)
// Lower arm has constraint, cannot overflow from 360 to 0, must go back around
struct Angle
{
	int Pitch;
	int Yaw;
};

/* Steppers */
#define FULLSTEP 4
#define HALFSTEP 8
#define STEPS 2038

AccelStepper stepperPitch(FULLSTEP, 8, 10, 9, 11);
AccelStepper stepperYaw(FULLSTEP, 4, 6, 5, 7);

/* Angles */

/* Input */
bool inputYaw = true;
String readString;

/* Setup */
void setup()
{
	Serial.begin(115200);

	// Stepper setup
	stepperPitch.setMaxSpeed(1000.0);
	stepperPitch.setAcceleration(100.0);
	stepperPitch.setSpeed(200);

	stepperYaw.setMaxSpeed(1000.0);
	stepperYaw.setAcceleration(100.0);
	stepperYaw.setSpeed(200);

	delay(3000);
}

int target = -1;
// Angle targets[6] = {{45, 45}, {90, 0}, {135, -90}, {180, -180}, {400, 0}, {0, 0}};
Angle targets[4] = {{90, 180}, {90, 355}, {90, 361}, {0, 0}};


bool returnHome = false;
long posP = 0;
long posY = 0;

void loop()
{
	// Workflow

	// 0. (setup) get local globe offset from device lat/lng and time

	// 1. Get global (lat/lng) position of target

	// 2. Get angle to global position from local position (relative to N/S & E/W)

	// WONTDO 3. Get angle of device heading relative to N/S & E/W with compass
	// assume facing true north on startup

	// DONE 5. Turn stepper angles into local device angles
	// pointAtLocal and angleToSteppedAngle

	// 6. Transform arm angle to global angle from #3

	// 7. Offset arm global angle with angle from #2 to point at target in global space

	// 8. Add interface to switch between multiple global targets

	// DONE 9. Handle arm reaching limit due to wire
	// constrainAngle
	

	if (Serial.available() && Serial.read() == 10)
	{
		returnHome = !returnHome;
		// target = target == -1 ? 0 : -1;
		// if (target == 0)
		// {
		// 	stepperPitch.enableOutputs();
		// 	stepperYaw.enableOutputs();
		// 	stepperPitch.setCurrentPosition(0);
		// 	stepperYaw.setCurrentPosition(0);
		// }
	}

	if (reachedTarget()) {
		disableMotors();
		delay(500);

		if (returnHome) {
			pointAtLocal({0,0});
			Serial.println("Returning home");
		} else {
			posP += 5;
			posY += 10;
			// short p = random(0, 360);
			// short y = random(0, 360);
			pointAtLocal({posP, posY});
		}
	}
	else {
		stepperPitch.run();
		stepperYaw.run();
	}
}

/* Movement control */
void pointAtLocal(Angle targetAngle)
{
	Angle targetAngleCapped = constrainAngle(targetAngle, 360);
	Angle targetStepAngle = angleToSteppedAngle(targetAngleCapped);

	Serial.print("Move to: ");
	Serial.print("(");
	Serial.print(targetAngleCapped.Pitch);
	Serial.print(", ");
	Serial.print(targetAngleCapped.Yaw);
	Serial.print(") ");
	Serial.print(targetStepAngle.Pitch);
	Serial.print(", ");
	Serial.print(targetStepAngle.Yaw);
	Serial.print(" | ");
	Serial.print(stepperPitch.currentPosition());
	Serial.print(", ");
	Serial.println(stepperYaw.currentPosition());

	stepperPitch.moveTo(targetStepAngle.Pitch);
	stepperYaw.moveTo(targetStepAngle.Yaw);
}

Angle globalToLocal(Angle globalAngle, int northOffsetAngle, int gravityOffset)
{
}


/* Math Helpers */
/* Return 0 - STEPS */
Angle angleToSteppedAngle(Angle angle)
{
	return {round(map(angle.Pitch, 0, 360, 0, STEPS)), round(map(angle.Yaw, 0, 360, 0, STEPS))};
}

/* Angle Helpers */
float constrainAngle360(float dta)
{
	dta = fmod(dta, 2.0 * PI);
	if (dta < 0.0)
		dta += 2.0 * PI;
	return dta;
}

Angle constrainAngle(Angle ang, int max)
{
	return { (int)(ang.Pitch - max * floor(ang.Pitch/max)), (int)(ang.Yaw - max * floor(ang.Yaw/max)) };
}

float angleDifference(int a, int b, int max)
{
	int c = a - b;
	float d = abs(c) % max;
	float r = d > max / 2 ? max - d : d;

	//calculate sign
	int sign = (c >= 0 && a - b <= max / 2) || (c <= -max / 2 && c >= -max) ? 1 : -1;
	r *= sign;

	return r;
}

/* Motor Helpers */
bool reachedTarget()
{
	return stepperPitch.distanceToGo() == 0 && stepperYaw.distanceToGo() == 0;
}

void disableMotors()
{
	stepperPitch.disableOutputs();
	stepperYaw.disableOutputs();
}
