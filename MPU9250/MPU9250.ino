#include "MPU9250.h"
#include "EEPROM.h"

/* magnetometer data */
float h, hx, hy, hz;
/* magnetometer calibration data */
float hxb, hxs, hyb, hys, hzb, hzs;
/* euler angles */
float yaw_rad, heading_rad;
/* filtered heading */
float filtered_heading_rad;
float window_size = 20;
/* conversion radians to degrees */
const float R2D = 180.0f / PI;
/* MPU 9250 object */
MPU9250 imu(Wire, 0x68);
/* MPU 9250 data ready pin */
// const uint8_t kMpu9250DrdyPin = 2;
/* Flag set to indicate MPU 9250 data is ready */
// volatile bool imu_data_ready = false;

/* ISR to set data ready flag */
// void data_ready()
// {
//   imu_data_ready = true;
// }

void setup()
{
  /* Serial for displaying results */
  Serial.begin(115200);
  while(!Serial) {}
  Serial.println("Starting");
  /* 
  * Start the sensor, set the bandwidth the 10 Hz, the output data rate to
  * 50 Hz, and enable the data ready interrupt. 
  */
  imu.begin();
  imu.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_10HZ);
  imu.setSrd(19);
//   imu.enableDataReadyInterrupt();
  /*
  * Load the magnetometer calibration
  */
  uint8_t eeprom_buffer[24];
  for (unsigned int i = 0; i < sizeof(eeprom_buffer); i++ ) {
    eeprom_buffer[i] = EEPROM.read(i);
  }
  memcpy(&hxb, eeprom_buffer, sizeof(hxb));
  memcpy(&hyb, eeprom_buffer + 4, sizeof(hyb));
  memcpy(&hzb, eeprom_buffer + 8, sizeof(hzb));
  memcpy(&hxs, eeprom_buffer + 12, sizeof(hxs));
  memcpy(&hys, eeprom_buffer + 16, sizeof(hys));
  memcpy(&hzs, eeprom_buffer + 20, sizeof(hzs));
  Serial.print(hxb); Serial.print("\t"); Serial.print(hxs); Serial.print("\t"); Serial.print(hyb); Serial.print("\t"); Serial.print(hys); Serial.print("\t"); Serial.print(hzb); Serial.print("\t");  Serial.println(hzs);
  imu.setMagCalX(hxb, hxs);
  imu.setMagCalY(hyb, hys);
  imu.setMagCalZ(hzb, hzs);
  /* Attach the data ready interrupt to the data ready ISR */
//   pinMode(kMpu9250DrdyPin, INPUT);
//   attachInterrupt(kMpu9250DrdyPin, data_ready, RISING);
}

void loop()
{
//   if (imu_data_ready) {
    // imu_data_ready = false;
    /* Read the MPU 9250 data */
    imu.readSensor();
    hx = imu.getMagX_uT();
    hy = imu.getMagY_uT();
    hz = imu.getMagZ_uT();
    /* Normalize magnetometer data */
    h = sqrtf(hx * hx + hy * hy + hz * hz);
    hx /= h;
    hy /= h;
    hz /= h;
    /* Compute euler angles */
    yaw_rad = atan2f(-hy, hx);
    heading_rad = constrainAngle360(yaw_rad);
    /* Filtering heading */
    filtered_heading_rad = (filtered_heading_rad * (window_size - 1.0f) + heading_rad) / window_size;
    /* Display the results */
    Serial.print(yaw_rad * R2D);
    Serial.print("\t");
    Serial.print(heading_rad * R2D);
    Serial.print("\t");
    Serial.println(filtered_heading_rad * R2D);
//   }
}

/* Bound angle between 0 and 360 */
float constrainAngle360(float dta) {
  dta = fmod(dta, 2.0 * PI);
  if (dta < 0.0)
    dta += 2.0 * PI;
  return dta;
}